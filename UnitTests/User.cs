﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace UnitTests
{
    class User
    {
        public string Nickname { get; private set; }
        public string FIO { get; private set; }
        public DateTime Birthday { get; private set; }
        public bool Gender { get; private set; }
        public string Phone_number { get; private set; }
        public string Email { get; private set; }
        public List<User> Friends { get; private set; }
        public List<User> Subscribed { get; private set; }
        public int DelectedPosts { get; private set; }
        public bool Banned { get; private set; }
        public DateTime BanDate { get; private set; }
        public DateTime BanUntilDate { get; private set; }
        public string BanMessage { get; private set; }
        public bool Root { get; private set; }
        public List<Post> Posts { get; private set; }
        public List<Comment> Comments { get; private set; }

        public User(string nickname, string fio, DateTime birthday, bool gender, string ph_number, string email, bool root)
        {
            Nickname = nickname;
            FIO = fio;
            Birthday = birthday;
            Gender = gender;
            Phone_number = ph_number;
            Email = email;
            Root = root;

            Friends = new List<User>();
            Subscribed = new List<User>();
            Posts = new List<Post>();
            Comments = new List<Comment>();
            DelectedPosts = 0;
            Banned = false;
            BanDate = new DateTime();
            BanUntilDate = new DateTime();
            BanMessage = "";
        }

        public int AddFriend(User user)
        {
            if (Friends.IndexOf(user) == -1)
                Friends.Add(user);
            return Friends.Count;
        }

        public int RemoveFriend(User user)
        {
            Friends.Remove(user);
            return Friends.Count;
        }

        public int Subscrebe(User user)
        {
            if (Subscribed.IndexOf(user) == -1)
                Subscribed.Add(user);
            return Subscribed.Count;
        }

        public int RemoveSubscrebe(User user)
        {
            Subscribed.Remove(user);
            return Subscribed.Count;
        }

        public int SetBan(DateTime date, DateTime until, string mes)
        {
            if (until < date)
                return -1;

            Banned = true;
            BanDate = date;
            BanUntilDate = until;
            BanMessage = mes;
            return 0;
        }

        public void UnBan()
        {
            Banned = false;
            BanDate = new DateTime();
            BanUntilDate = new DateTime();
            BanMessage = "";
        }

        public int IncDeletedPosts()
        {
            return ++DelectedPosts;
        }

        public void ResetDeletedPosts()
        {
            DelectedPosts = 0;
        }

        public int AddPost(string text, DateTime date, User author, List<Image> pictures = null, List<string> categories = null, Dictionary<string, string> myFields = null)
        {
            Post post = new Post(text, date, author, pictures, categories, myFields);
            if (Posts.FirstOrDefault(a => a.Equals(post)) == null)
                Posts.Add(post);
            return Posts.Count;
        }

        public int RemovePost(Post post)
        {
            Posts.Remove(post);
            return Posts.Count;
        }

        public int AddComment(Comment comment)
        {
            if (Comments.IndexOf(comment) == -1)
                Comments.Add(comment);
            return Comments.Count;
        }

        public int RemoveComment(Comment comment)
        {
            Comments.Remove(comment);
            return Comments.Count;
        }
    }
}

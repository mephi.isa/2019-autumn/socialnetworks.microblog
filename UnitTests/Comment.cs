﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace UnitTests
{
    class Comment: IEquatable<Comment>
    {
        public string Text { get; set; }
        public DateTime Date { get; protected set; }
        public User Author { get; protected set; }
        public List<Image> Pictures { get; set; }

        public Comment(string text, DateTime date, User author, List<Image> pictures = null)
        {
            Text = text;
            Date = date;
            Author = author;

            if (pictures != null)
                Pictures = pictures;
            else
                Pictures = new List<Image>();
        }

        public int AddPicture(Image image)
        {
            Pictures.Add(image);
            return Pictures.Count;
        }

        public bool Equals(Comment other)
        {
            if (other == null)
                return false;

            if (Pictures.Count != other.Pictures.Count)
                return false;

            for (int i = 0; i < Pictures.Count; i++)
            {
                if (ImageToBase64(Pictures[i]) != ImageToBase64(other.Pictures[i]))
                    return false;
            }

            return (Text == other.Text && Date == other.Date && Author == other.Author);
        }

        protected string ImageToBase64(Image image)
        {
            using (MemoryStream m = new MemoryStream())
            {
                image.Save(m, image.RawFormat);
                byte[] imageBytes = m.ToArray();
                return Convert.ToBase64String(imageBytes); ;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace UnitTests
{
    class Program
    {
        const string ExampleNick = "Nick";
        const string ExampleFIO = "Иванов Иван Иванович";
        static DateTime ExampleBirthday = new DateTime(1990, 1, 1);
        const bool ExampleGender = true;
        const string ExamplePhoneNumber = "+79161234567";
        const string ExampleEmail = "nick@mail.ru";
        const bool ExampleRoot = false;

        const string ExampleOtherNick = "Dock";
        const string ExampleOtherFIO = "Петрова Анна Петровна";
        static DateTime ExampleOtherBirthday = new DateTime(1993, 3, 11);
        const bool ExampleOtherGender = false;
        const string ExampleOtherPhoneNumber = "+79162345678";
        const string ExampleOtherEmail = "dock@mail.ru";
        const bool ExampleOtherRoot = true;

        static DateTime banDate = DateTime.Now;
        static DateTime banUntil = DateTime.Now.AddMonths(1);
        const string ExampleMesBan = "Тестовый бан";

        const string ExampleImageName = "1.png";
        const string ExampleOtherImageName = "2.png";
        const string ExampleCommentText = "Комментарий о статье";
        static DateTime ExampleCommentDateCreate = new DateTime(2019, 10, 16, 15, 20, 0);

        const string ExamplePostText = "Блаблабла очень интересная статья";
        static DateTime ExamplePostDateCreate = new DateTime(2019, 10, 16, 14, 40, 0);

        const string ExampleCategory = "Природа";
        const string ExampleFieldKey = "Ландшафт";
        const string ExampleFieldValue = "Горы";

        const string ExampleNewField = "New_field";
        const string ExampleNewValue = "New_value";
        const string ExampleNotDefinedField = "not_defined";

        static int Sum = 0, Success = 0, Fail = 0;

        static void Main(string[] args)
        {
            TestUser();
            Console.WriteLine();
            TestComment();
            Console.WriteLine();
            TestPost();

            Console.WriteLine();
            Console.WriteLine("Результат: ");
            Console.WriteLine("Всего тестов: {0}, Успешно: {1}, Ошибок: {2}.", Sum, Success, Fail);

            Console.ReadKey();
        }

        static void TestUser()
        {
            User user1 = new User(ExampleNick, ExampleFIO, ExampleBirthday, ExampleGender, ExamplePhoneNumber, ExampleEmail, ExampleRoot);
            User user2 = new User(ExampleOtherNick, ExampleOtherFIO, ExampleOtherBirthday, ExampleOtherGender, ExampleOtherPhoneNumber, ExampleOtherEmail, ExampleOtherRoot);
            Post post = new Post(ExamplePostText, ExamplePostDateCreate, user1);
            Comment comment = new Comment(ExampleCommentText, ExampleCommentDateCreate, user2);

            Console.WriteLine("Тестируем класс User:");

            Console.WriteLine("Конструктор и gets: ");
            Console.Write("\tНик: ");
            Compare(user1.Nickname, ExampleNick);
            Console.Write("\tФИО: ");
            Compare(user1.FIO, ExampleFIO);
            Console.Write("\tДата рождения: ");
            Compare(user1.Birthday, ExampleBirthday);
            Console.Write("\tПол: ");
            Compare(user1.Gender, ExampleGender);
            Console.Write("\tТелефон: ");
            Compare(user1.Phone_number, ExamplePhoneNumber);
            Console.Write("\tEmail: ");
            Compare(user1.Email, ExampleEmail);
            Console.Write("\tДрузья: ");
            Compare(user1.Friends.Count, 0);
            Console.Write("\tПодписки: ");
            Compare(user1.Subscribed.Count, 0);
            Console.Write("\tПосты: ");
            Compare(user1.Posts.Count, 0);
            Console.Write("\tКомментарии: ");
            Compare(user1.Comments.Count, 0);
            Console.Write("\tУдаленные посты: ");
            Compare(user1.DelectedPosts, 0);
            Console.Write("\tБан - статус: ");
            Compare(user1.Banned, false);
            Console.Write("\tДата бана: ");
            Compare(user1.BanDate, new DateTime());
            Console.Write("\tДата разбана: ");
            Compare(user1.BanUntilDate, new DateTime());
            Console.Write("\tСообщение о бане: ");
            Compare(user1.BanMessage, "");
            Console.Write("\tРут-права: ");
            Compare(user1.Root, ExampleRoot);

            Console.Write("Добавление друга (еще нет в списке): ");
            user1.AddFriend(user2);
            Compare(user1.Friends.IndexOf(user2), 0);

            Console.Write("Добавление друга (уже есть в списке): ");
            user1.AddFriend(user2);
            PrintResult(Compare(user1.Friends.IndexOf(user2), 0, false) && Compare(user1.Friends.Count, 1, false));

            Console.Write("Удаление друга: ");
            user1.RemoveFriend(user2);
            Compare(user1.Friends.IndexOf(user2), -1);

            Console.Write("Добавление подписки (еще нет в списке): ");
            user1.Subscrebe(user2);
            Compare(user1.Subscribed.IndexOf(user2), 0);

            Console.Write("Добавление подписки (уже есть в списке): ");
            user1.Subscrebe(user2);
            PrintResult(Compare(user1.Subscribed.IndexOf(user2), 0, false) && Compare(user1.Subscribed.Count, 1, false));

            Console.Write("Удаление подписки: ");
            user1.RemoveSubscrebe(user2);
            Compare(user1.Subscribed.IndexOf(user2), -1) ;

            Console.Write("Бан пользователя (текущая дата больше дата разбана): ");
            int res = user1.SetBan(banDate.AddSeconds(1), banDate, ExampleMesBan);
            PrintResult(Compare(user1.Banned, false, false) && Compare(user1.BanDate, new DateTime(), false) && Compare(user1.BanUntilDate, new DateTime(), false) && Compare(user1.BanMessage, "", false) && Compare(res, -1, false));

            Console.Write("Бан пользователя (корректные данные): ");
            res = user1.SetBan(banDate, banUntil, ExampleMesBan);
            PrintResult(Compare(user1.Banned, true, false) && Compare(user1.BanDate, banDate, false) && Compare(user1.BanUntilDate, banUntil, false) && Compare(user1.BanMessage, ExampleMesBan, false) && Compare(res, 0, false));

            Console.Write("Разбан пользователя: ");
            user1.UnBan();
            PrintResult(Compare(user1.Banned, false, false) && Compare(user1.BanDate, new DateTime(), false) && Compare(user1.BanUntilDate, new DateTime(), false) && Compare(user1.BanMessage, "", false));

            Console.Write("Увеличение счетчика удаленных постов: ");
            user1.IncDeletedPosts();
            Compare(user1.DelectedPosts, 1);

            Console.Write("Сброс счетчика удаленных постов: ");
            user1.ResetDeletedPosts();
            Compare(user1.DelectedPosts, 0);

            Console.Write("Добавление поста (еще нет в списке): ");
            user1.AddPost(ExamplePostText, ExamplePostDateCreate, user1);
            Compare(user1.Posts.IndexOf(new Post(ExamplePostText, ExamplePostDateCreate, user1)), 0);

            Console.Write("Добавление поста (уже есть в списке): ");
            user1.AddPost(ExamplePostText, ExamplePostDateCreate, user1);
            PrintResult(Compare(user1.Posts.IndexOf(new Post(ExamplePostText, ExamplePostDateCreate, user1)), 0, false) && Compare(user1.Posts.Count, 1, false));

            Console.Write("Удаление поста: ");
            user1.RemovePost(post);
            Compare(user1.Posts.IndexOf(post), -1);

            Console.Write("Добавление комментария  (еще нет в списке): ");
            user1.AddComment(comment);
            Compare(user1.Comments.IndexOf(comment), 0);

            Console.Write("Добавление комментария (уже есть в списке): ");
            user1.AddComment(comment);
            PrintResult(Compare(user1.Comments.IndexOf(comment), 0, false) && Compare(user1.Comments.Count, 1, false));

            Console.Write("Удаление комментария: ");
            user1.RemoveComment(comment);
            Compare(user1.Comments.IndexOf(comment), -1);
        }

        static void TestComment()
        {
            User user1 = new User(ExampleNick, ExampleFIO, ExampleBirthday, ExampleGender, ExamplePhoneNumber, ExampleEmail, ExampleRoot);
            User user2 = new User(ExampleOtherNick, ExampleOtherFIO, ExampleOtherBirthday, ExampleOtherGender, ExampleOtherPhoneNumber, ExampleOtherEmail, ExampleOtherRoot);
            Comment comment1 = new Comment(ExampleCommentText, ExampleCommentDateCreate, user1);
            Comment comment2 = new Comment(ExampleCommentText, ExampleCommentDateCreate, user1, new List<Image> { Image.FromFile(ExampleOtherImageName) });

            Console.WriteLine("Тестируем класс Comment:");

            Console.WriteLine("Конструктор и gets: ");
            Console.Write("\tТекст: ");
            Compare(comment1.Text, ExampleCommentText);
            Console.Write("\tДата: ");
            Compare(comment1.Date, ExampleCommentDateCreate);
            Console.Write("\tАвтор: ");
            Compare(comment1.Author, user1);
            Console.Write("\tКартинки: ");
            Compare(comment1.Pictures.Count, 0);
            Console.Write("\tКартинки (повторно): ");
            PrintResult(Compare(comment2.Pictures.Count, 1, false) && Compare(ImageToBase64(comment2.Pictures[0]), ImageToBase64(Image.FromFile(ExampleOtherImageName)), false));

            Console.Write("Добавление изображения: ");
            comment1.AddPicture(Image.FromFile(ExampleImageName));
            Compare(ImageToBase64(comment1.Pictures[0]), ImageToBase64(Image.FromFile(ExampleImageName)));

            Console.WriteLine("Проверка на равенство:");
            Comment newComment;

            Console.Write("\tПроверка на равенство 1 (сравнение с нулем): ");
            Compare(comment1.Equals(null), false);

            Console.Write("\tПроверка на равенство 2 (разное кол-во изображений): ");
            newComment = new Comment(comment1.Text, comment1.Date, comment1.Author);
            Compare(comment1.Equals(newComment), false);

            Console.Write("\tПроверка на равенство 3 (разные изображения): ");
            newComment = new Comment(comment1.Text, comment1.Date, comment1.Author, new List<Image>() { Image.FromFile(ExampleOtherImageName)});
            Compare(comment1.Equals(newComment), false);

            Console.Write("\tПроверка на равенство 4 (разный текст): ");
            newComment = new Comment(comment1.Text + "1", comment1.Date, comment1.Author, comment1.Pictures);
            Compare(comment1.Equals(newComment), false);

            Console.Write("\tПроверка на равенство 5 (разные даты): ");
            newComment = new Comment(comment1.Text, comment1.Date.AddSeconds(1), comment1.Author, comment1.Pictures);
            Compare(comment1.Equals(newComment), false);

            Console.Write("\tПроверка на равенство 6 (разные авторы): ");
            newComment = new Comment(comment1.Text, comment1.Date, user2, comment1.Pictures);
            Compare(comment1.Equals(newComment), false);

            Console.Write("\tПроверка на равенство 7 (равенство верно): ");
            newComment = new Comment(comment1.Text, comment1.Date, comment1.Author, comment1.Pictures);
            Compare(comment1.Equals(newComment), true);
        }

        static void TestPost()
        {
            User user1 = new User(ExampleNick, ExampleFIO, ExampleBirthday, ExampleGender, ExamplePhoneNumber, ExampleEmail, ExampleRoot);
            User user2 = new User(ExampleOtherNick, ExampleOtherFIO, ExampleOtherBirthday, ExampleOtherGender, ExampleOtherPhoneNumber, ExampleOtherEmail, ExampleOtherRoot);
            Post post1 = new Post(ExamplePostText, ExamplePostDateCreate, user1);
            Post post2 = new Post(ExamplePostText, ExamplePostDateCreate, user1, new List<Image> { Image.FromFile(ExampleImageName) }, new List<string> { ExampleCategory }, new Dictionary<string, string> { [ExampleFieldKey] = ExampleFieldValue });

            Console.WriteLine("Тестируем класс Post:");

            Console.WriteLine("Конструктор и gets: ");
            Console.Write("\tТекст: ");
            Compare(post1.Text, ExamplePostText);
            Console.Write("\tДата: ");
            Compare(post1.Date, ExamplePostDateCreate);
            Console.Write("\tАвтор: ");
            Compare(post1.Author, user1);
            Console.Write("\tКартинки: ");
            Compare(post1.Pictures.Count, 0);
            Console.Write("\tКатегории: ");
            Compare(post1.Categories.Count, 0);
            Console.Write("\tКастомные поля: ");
            Compare(post1.MyFields.Count, 0);
            Console.Write("\tКомментарии: ");
            Compare(post1.Comments.Count, 0);
            Console.Write("\tКартинки (повторно): ");
            PrintResult(Compare(post2.Pictures.Count, 1, false) && Compare(ImageToBase64(post2.Pictures[0]), ImageToBase64(Image.FromFile(ExampleImageName)), false));
            Console.Write("\tКатегории (повторно): ");
            PrintResult(Compare(post2.Categories.Count, 1, false) && Compare(post2.Categories[0], ExampleCategory, false));
            Console.Write("\tКастомные поля (повторно): ");
            PrintResult(Compare(post2.MyFields.Count, 1, false) && Compare(post2.MyFields.Keys.FirstOrDefault(), ExampleFieldKey, false) && Compare(post2.MyFields.Values.FirstOrDefault(), ExampleFieldValue, false));

            Console.Write("Добавление изображения: ");
            post1.AddPicture(Image.FromFile(ExampleImageName));
            Compare(ImageToBase64(post1.Pictures[0]), ImageToBase64(Image.FromFile(ExampleImageName)));

            Console.Write("Добавление кастомного поля (еще нет в списке): ");
            post1.AddField(ExampleNewField, ExampleNewValue);
            PrintResult(Compare(post1.MyFields.Keys.FirstOrDefault(a => a == ExampleNewField), ExampleNewField, false) && Compare(post1.MyFields.Values.FirstOrDefault(a => a == ExampleNewValue), ExampleNewValue, false));

            Console.Write("Добавление кастомного поля (уже есть в списке): ");
            post1.AddField(ExampleNewField, ExampleNewValue);
            PrintResult(Compare(post1.MyFields.Count, 1, false) && Compare(post1.MyFields.Keys.FirstOrDefault(a => a == ExampleNewField), ExampleNewField, false) && Compare(post1.MyFields.Values.FirstOrDefault(a => a == ExampleNewValue), ExampleNewValue, false));

            Console.Write("Получение существующего кастомного поля по ключу: ");
            Compare(post1.GetFieldByKey(ExampleNewField), ExampleNewValue);

            Console.Write("Получение несуществующего кастомного поля по ключу: ");
            Compare(post1.GetFieldByKey(ExampleNotDefinedField), "");

            Console.Write("Добавление категории (еще нет в списке): ");
            post1.AddCategory(ExampleCategory);
            Compare(post1.Categories.IndexOf(ExampleCategory), 0);

            Console.Write("Добавление категории (уже есть в списке): ");
            post1.AddCategory(ExampleCategory);
            PrintResult(Compare(post1.Categories.Count, 1, false) && Compare(post1.Categories.IndexOf(ExampleCategory), 0, false));

            Console.Write("Добавление комментария (еще нет в списке): ");
            post1.AddComment(ExampleCommentText, ExampleCommentDateCreate, user1, new List<Image>() { Image.FromFile(ExampleImageName) });
            Compare(post1.Comments.IndexOf(new Comment(ExampleCommentText, ExampleCommentDateCreate, user1, new List<Image>() { Image.FromFile(ExampleImageName) })), 0);

            Console.Write("Добавление комментария (уже есть в списке): ");
            post1.AddComment(ExampleCommentText, ExampleCommentDateCreate, user1, new List<Image>() { Image.FromFile(ExampleImageName) });
            PrintResult(Compare(post1.Comments.Count, 1, false) && Compare(post1.Comments.IndexOf(new Comment(ExampleCommentText, ExampleCommentDateCreate, user1, new List<Image>() { Image.FromFile(ExampleImageName) })), 0, false));

            Console.Write("Удаление комментария: ");
            post1.RemoveComment(new Comment(ExampleCommentText, ExampleCommentDateCreate, user1, new List<Image>() { Image.FromFile(ExampleImageName) }));
            Compare(post1.Comments.IndexOf(new Comment(ExampleCommentText, ExampleCommentDateCreate, user1, new List<Image>() { Image.FromFile(ExampleImageName) })), -1);

            Console.WriteLine("Удаление всех комментариев: ");
            Console.Write("\tДобавление двух комментариев: ");
            post1.AddComment(ExampleCommentText, ExampleCommentDateCreate, user2);
            post1.AddComment("Блаблабла", new DateTime(2019, 10, 16, 15, 20, 0), user2);
            if (Compare(post1.Comments.Count, 2))
            {
                Console.Write("\tУдаление комментариев: ");
                post1.RemoveAllComment();
                Compare(post1.Comments.Count, 0);
            }

            Console.WriteLine("Проверка на равенство:");
            Post newPost;

            Console.Write("\tПроверка на равенство 1 (сравнение с нулем): ");
            Compare(post2.Equals(null), false);

            Console.Write("\tПроверка на равенство 2 (разный текст): ");
            newPost = new Post(post2.Text + "1", post2.Date, post2.Author, post2.Pictures, post2.Categories, post2.MyFields);
            Compare(post2.Equals(newPost), false);

            Console.Write("\tПроверка на равенство 3 (разные даты): ");
            newPost = new Post(post2.Text, post2.Date.AddSeconds(1), post2.Author, post2.Pictures, post2.Categories, post2.MyFields);
            Compare(post2.Equals(newPost), false);

            Console.Write("\tПроверка на равенство 4 (разные авторы): ");
            newPost = new Post(post2.Text, post2.Date, user2, post2.Pictures, post2.Categories, post2.MyFields);
            Compare(post2.Equals(newPost), false);

            Console.Write("\tПроверка на равенство 5 (разное кол-во изображений): ");
            newPost = new Post(post2.Text, post2.Date, post2.Author, null, post2.Categories, post2.MyFields);
            Compare(post2.Equals(newPost), false);

            Console.Write("\tПроверка на равенство 6 (разные изображения): ");
            newPost = new Post(post2.Text, post2.Date, post2.Author, new List<Image>() { Image.FromFile(ExampleOtherImageName) }, post2.Categories, post2.MyFields);
            Compare(post2.Equals(newPost), false);

            Console.Write("\tПроверка на равенство 7 (разное кол-во категорий): ");
            newPost = new Post(post2.Text, post2.Date, post2.Author, post2.Pictures, null, post2.MyFields);
            Compare(post2.Equals(newPost), false);

            Console.Write("\tПроверка на равенство 8 (разные категории): ");
            newPost = new Post(post2.Text, post2.Date, post2.Author, post2.Pictures, new List<string> { ExampleCategory + "1" }, post2.MyFields);
            Compare(post2.Equals(newPost), false);

            Console.Write("\tПроверка на равенство 9 (разное кол-во кастомных полей): ");
            newPost = new Post(post2.Text, post2.Date, post2.Author, post2.Pictures, post2.Categories, null);
            Compare(post2.Equals(newPost), false);

            Console.Write("\tПроверка на равенство 10 (разные ключи кастомных полей): ");
            newPost = new Post(post2.Text, post2.Date, post2.Author, post2.Pictures, post2.Categories, new Dictionary<string, string> { [ExampleFieldKey + "1"] = ExampleFieldValue });
            Compare(post2.Equals(newPost), false);

            Console.Write("\tПроверка на равенство 11 (разные значения кастомных полей по ключу): ");
            newPost = new Post(post2.Text, post2.Date, post2.Author, post2.Pictures, post2.Categories, new Dictionary<string, string> { [ExampleFieldKey] = ExampleFieldValue + "1" });
            Compare(post2.Equals(newPost), false);

            Console.Write("\tПроверка на равенство 12 (равенство верно): ");
            newPost = new Post(post2.Text, post2.Date, post2.Author, post2.Pictures, post2.Categories, post2.MyFields);
            Compare(post2.Equals(newPost), true);
        }

        static bool Compare(object obj1, object obj2, bool Print_res = true)
        {
            bool res = Equals(obj1, obj2);
            if (res)
            {
                if (Print_res)
                    PrintResult(res);
            }
            else
            {
                if (Print_res)
                    PrintResult(res);
            }

            return res;
        }

        static void PrintResult(bool Res)
        {
            Sum++;

            if (Res)
            {
                Success++;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Успешно");
                Console.ResetColor();
            }
            else
            {
                Fail++;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ошибка");
                Console.ResetColor();
            }
        }

        static string ImageToBase64(Image image)
        {
            using (MemoryStream m = new MemoryStream())
            {
                image.Save(m, image.RawFormat);
                byte[] imageBytes = m.ToArray();
                return Convert.ToBase64String(imageBytes); ;
            }
        }
    }
}

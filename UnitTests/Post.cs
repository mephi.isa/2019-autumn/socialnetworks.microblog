﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Linq;

namespace UnitTests
{
    class Post : Comment, IEquatable<Post>
    {
        public List<string> Categories { get; set; }

        public Dictionary<string, string> MyFields { get; set; }

        public List<Comment> Comments { get; private set; }

        public Post(string text, DateTime date, User author, List<Image> pictures = null, List<string> categories = null, Dictionary<string, string> myFields = null) : base(text, date, author, pictures)
        {
            if (categories != null)
                Categories = categories;
            else
                Categories = new List<string>();

            if (myFields != null)
                MyFields = myFields;
            else
                MyFields = new Dictionary<string, string>();

            Comments = new List<Comment>();
        }

        public int AddField(string key, string value)
        {
            try
            {
                MyFields.Add(key, value);
            }
            catch(Exception ex)
            {
                return -1;
            }

            return MyFields.Count;
        }

        public string GetFieldByKey(string key)
        {
            string value;
            MyFields.TryGetValue(key, out value);
            return value == null ? "" : value;
        }

        public int AddCategory(string category)
        {
            if (Categories.IndexOf(category) == -1)
                Categories.Add(category);
            return Categories.Count;
        }

        public int AddComment(string text, DateTime date, User author, List<Image> pictures = null)
        {
            Comment comment = new Comment(text, date, author, pictures);
            if (Comments.FirstOrDefault(a => a.Equals(comment)) == null)
                Comments.Add(comment);
            return Comments.Count;
        }

        public int RemoveComment(Comment comment)
        {
            Comments.Remove(comment);
            return Comments.Count;
        }

        public void RemoveAllComment()
        {
            Comments.Clear();
        }

        public bool Equals(Post other)
        {
            if (other == null)
                return false;

            if (Pictures.Count != other.Pictures.Count)
                return false;

            for (int i = 0; i < Pictures.Count; i++)
            {
                if (ImageToBase64(Pictures[i]) != ImageToBase64(other.Pictures[i]))
                    return false;
            }

            return (Text == other.Text && Date == other.Date && Author == other.Author && Enumerable.SequenceEqual(Categories, other.Categories) && Enumerable.SequenceEqual(MyFields, other.MyFields));
        }
    }
}
